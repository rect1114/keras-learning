import keras
from keras.utils import plot_model

model = keras.models.load_model('model_trained')

plot_model(model,to_file='model.png',show_shapes=True,show_layer_names=True)